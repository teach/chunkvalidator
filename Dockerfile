FROM golang:alpine as gobuild


WORKDIR /go/src/git.nemunai.re/chunkvalidator


COPY *.go go.mod go.sum ./

RUN go get -d -v && \
    go build -v -ldflags="-s -w"


FROM alpine

EXPOSE 8081

CMD ["chunkvalidator"]

COPY --from=gobuild /go/src/git.nemunai.re/chunkvalidator/chunkvalidator /usr/sbin/chunkvalidator